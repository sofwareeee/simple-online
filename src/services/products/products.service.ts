
import { IProduct } from '../../models/Product.interface';
import axios from '../../utilities/axios';
import { loadAbort } from '../../utilities/functions';
  
export const getProducts = () => {
    const controller = loadAbort();

    return { call: axios.get<IProduct>(`/products`, { signal: controller.signal }), 
            controller 
    }
}

export const getProduct = (id: string) => {
  const controller = loadAbort();

  return { call: axios.get<IProduct>(`/products/${id}`, { signal: controller.signal }), 
          controller 
  }
}

    
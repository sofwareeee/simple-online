import styled from 'styled-components';
import { motion } from 'framer-motion';

export const Container = styled.div`
  position: fixed;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 9999;
`;

export const MinicartContainer = styled(motion.div)`
  width: 440px;
  position: fixed;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 9999;
  background: #fff;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const Header = styled.div`
  padding-bottom: 20px !important;
  border-bottom: 1px solid #0000000d;
  padding: 20px;
  display: flex;
  justify-content: space-between;

  h3 {
    font-size: 20px;
  }

  svg {
    width: 20px;
    height: 20px;
    cursor: pointer;
  }
`;

export const Body = styled.div`
  height: 100%;
  position: relative;
  overflow-x: hidden;

  @media (max-width: 768px) {
    > div > div:first-child > div {
      width: 100%;
      position: relative;
    }
  }

  > div {
    width: 100% !important;
  }

  > img {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
  }
`;

export const Footer = styled.div`
  padding: 20px;
  > div {
    border-radius: 20px;
    background-color: #0000000d;
    padding: 20px;

    h3 {
      font-size: 18px;
    }

    > p {
      font-size: 16px;
      margin: 10px 0 !important;
    }

    ul {
      display: flex;
      justify-content: space-between;
      list-style: none;
      li {
        font-weight: bold;
        font-size: 16px;
      }
    }
  }
`;

export const Actions = styled.div`
  padding: 20px;
  padding-top: 0;
  margin-bottom: 10px;
  display: flex;
  button {
    padding: 13px 0;
    @media (max-width: 768px) {
      font-size: 19px !important;
    }
  }
`;

export const Overlay = styled.div`
  background-color: #0000009c;
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;
import { useContext, useEffect, useState } from 'react';
import { MdClose } from 'react-icons/md';
import { MinicartContext } from '../../../../context/MinicartContext';
import { ProductCard, getQuantityMinicart } from '../..';
import { TYPE_CARD } from '../../../../utilities/constants';
import { IProduct } from '../../../../models/Product.interface';
import { STORE } from '../Products/Utilities/constants';
import { useCustomDispatch, useCustomSelector } from '../../../../hooks/useRedux';
import { ButtonPrimary, ButtonSimple } from '../../../../components/Buttons';
import { calculateTotal } from '../../../../utilities/functions';
import { removeAll } from '../../../../redux/slices/cart';
import {
  Container,
  MinicartContainer,
  Header,
  Body,
  Footer,
  Actions,
  Overlay,
} from './Minicart.styles.ts';

export const Minicart = () => {
  const DOM_BODY = document.getElementsByTagName('html')[0];
  const { openMinicart, setOpenMinicart } = useContext(MinicartContext) as any;
  const { cart } = useCustomSelector((state) => state);
  const [quantityProducts, setQuantityProducts] = useState(0);
  const dispatch = useCustomDispatch();

  useEffect(() => {
    if (openMinicart) {
      DOM_BODY.style.overflow = 'hidden';
    } else {
      DOM_BODY.style.overflow = 'initial';
    }
  }, [openMinicart]);

  useEffect(() => {
    setQuantityProducts(getQuantityMinicart(cart.cart));
  }, [cart]);

  const handleCloseMinicart = () => {
    setOpenMinicart(false);
  };

  return (
    <Container>
      <MinicartContainer
        initial={{ x: 500 }}
        animate={{ x: 0 }}
        transition={{ duration: 0.5 }}
        exit={{ x: 500 }}
      >
        <Header>
          <h3>Carrito de compras</h3>
          <MdClose onClick={handleCloseMinicart} />
        </Header>
        <Body>
          {cart.cart?.length > 0 ? (
            cart.cart?.map((product: IProduct) => (
              <ProductCard
                key={product.id}
                product={product}
                style={{ width: '250px' }}
                type={TYPE_CARD.HORIZONTAL}
                location={STORE.MINICART}
              />
            ))
          ) : (
            <img
              src="https://anaku-mode.web.app/assets/img/cart_empty.svg"
              alt="Cart Empty"
            />
          )}
        </Body>
        <Footer>
          <div>
            <h3>Resumen de Pedido</h3>
            <p>Tienes {quantityProducts} productos en tu carrito.</p>
            <ul>
              <li>Subtotal</li>
              <li>S/ {calculateTotal(cart.cart)}</li>
            </ul>
          </div>
        </Footer>
        <Actions>
          <ButtonSimple onClick={handleCloseMinicart}>Seguir comprando</ButtonSimple>
          <ButtonPrimary
            onClick={() => dispatch(removeAll())}
            disabled={cart.cart?.length <= 0}
          >
            Vaciar carrito
          </ButtonPrimary>
        </Actions>
      </MinicartContainer>
      <Overlay onClick={handleCloseMinicart} />
    </Container>
  );
};

import { ProductDescription, ProductName } from '../../Components'

import Styles from './ProductDescription.module.scss'
interface Props extends React.ComponentPropsWithRef<'div'> {
  name: string
  description: string
  size: string | undefined
  location  : string
  topping   : string[] | any
}

export const ProductDescriptionContainer = ({name, description, size, location, topping, ...props}: Props) => {
  
  const {container} = Styles
  return (
    <div className={container} {...props}>
      <ProductName name={name} size={size || ''} />
      <ProductDescription description={description} />
    </div>
  )
}

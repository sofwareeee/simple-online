import {useContext} from 'react'

import { TYPE_CARD } from '../../../../../../utilities/constants'
import { Card, ProductSummary } from '../../Components'

import { ProductImage } from '../../Components/ProductImage/ProductImage'
import { STORE } from '../../Utilities/constants'
import { ProductDescriptionContainer } from '../ProductDescriptionContainer/ProductDescription'
import { ProductSelectedContex } from '../../../../../../context/ProductSelectedContextProvider'

interface Props extends React.ComponentPropsWithRef<'div'> {}

interface Props {
  product: any
  type: string
  location?: string
}

export const ProductCard = ({product, type = TYPE_CARD.VERTICAL, location = STORE.STORE, ...props}: Props) => {
  const {setProductId, setShowModalProduct} = useContext(ProductSelectedContex) as any

  const onToggleModalProduct = () =>{
    setShowModalProduct(true)
    setProductId(product.id)
  }
  
  return (
    <Card {...props} type={type}>
      <ProductImage 
          id={product.id} image={product.data ? product.data.image : product.image} location={location} topping={product.content}
        onClick={() => location !== STORE.MINICART && onToggleModalProduct()} style={{cursor: 'pointer'}} />
      <div>
        <ProductDescriptionContainer 
          name={product.data ? product.data.name : product.name} 
          description={product.data ? product.data.description : product.description} 
          size={product.data ? product.data.size : product.size} 
          topping={product}
          location={location}
          onClick={() => location !== STORE.MINICART && onToggleModalProduct()}
          style={{cursor: 'pointer'}}  />
        <ProductSummary id={product.id} type={location} product={product.data ? product.data : product } />
      </div>
    </Card>
  )
}

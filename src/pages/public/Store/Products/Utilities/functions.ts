import { IProduct } from "../../../../../models/Product.interface"
import { STORAGE } from "../../../../../utilities/constants"

export const setStorageMinicart = (name: string, data: any) => {
    localStorage.setItem(name, JSON.stringify(data))
}

export const filterProduct = (products: IProduct[], id: string) => {
    return products.filter( (product: IProduct) => {
        if (product.id === id) {
            product.quantity = 1
            return product
        }
    } )
}

export const clearMinicart = () => {
    localStorage.removeItem(STORAGE.MINICART)
}

export const getProductInStorage = (name: string) => {
    const storage_cart = localStorage.getItem(name)
    if (storage_cart) return JSON?.parse(storage_cart)
    return null
}

export const saveProductInStorage = (product: IProduct[]) => {
    const productSelected = product[0]
    const storage_cart = localStorage.getItem(STORAGE.MINICART)

    if (!storage_cart) return setStorageMinicart(STORAGE.MINICART, product)

    const parseMinicart = JSON.parse(storage_cart)

    const accumProducts = []
    
    if (!parseMinicart.includes(product[0].id)) {
        accumProducts.push(...JSON?.parse(storage_cart), productSelected)
    }

    setStorageMinicart(STORAGE.MINICART, accumProducts)
}

export const getQuantityMinicart = (minicart: IProduct[]) => {
    let countProducts = 0
    
    if (minicart) {
        minicart.map( (product: IProduct) => {
            countProducts += product.quantity
        })
    }
    
    return countProducts
}

export const calculatePrice = (price: string | number | undefined, discount: string | number | undefined) => {

    if (discount) {
        return (Number(price) - (Number(price) * (Number(discount)/ 100))).toFixed(2)
    }
    return price
}
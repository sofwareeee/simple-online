
import Styles from './ProductName.module.scss'

interface Props {
  name: string
  size: string
}

export const ProductName = ({name, size}: Props) => {
    const {container} = Styles
  return (
    <h3 className={container}>{name} <small>({size})</small></h3>
  )
}

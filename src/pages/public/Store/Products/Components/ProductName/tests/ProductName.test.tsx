import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ProductName } from '../ProductName';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('ProductName Component', () => {
  it('render product name', () => {
    const wrapper = shallow(<ProductName name="Product Name" size="Large" />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render product with name and size', () => {
    const wrapper = shallow(<ProductName name="Product Name" size="Large" />);
    expect(wrapper.find('h3').text()).toEqual('Product Name (Large)');
  });
});
import { useContext, useState } from 'react'

import { BiLoaderAlt } from 'react-icons/bi'
import { HiOutlineTrash } from 'react-icons/hi'

import Styles from './ProductSummary.module.scss'
import { ButtonPrimary, ButtonSimple } from '../../../../../../components/Buttons'
import { IProduct } from '../../../../../../models/Product.interface'
import { calculatePrice } from '../../Utilities/functions'

import { useCustomDispatch, useCustomSelector } from '../../../../../../hooks/useRedux'
import { AlertContext } from '../../../../../../context/AlertContext'
import useFetchAndLoad from '../../../../../../hooks/useFetchAndLoad'
import { getProduct } from '../../../../../../services/products/products.service'
import { addToCart } from '../../../../../../redux/slices/cart'
import { MinicartContext } from '../../../../../../context/MinicartContext'
import { AnimatePresence } from 'framer-motion'
import { ModalActions } from '..'

interface Props {
  id: string
  type?: string
  product?: IProduct
}

export const ProductSummary = ({id, product}: Props) => {
  const {container, containerActions, iconRotate} = Styles
  const [isProcessing, setisProcessing] = useState(false)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [hasActions, setHasActions] = useState(true)  

  const { cart }: any = useCustomSelector((state) => state);
  const {setAlertConfig} = useContext(AlertContext) as any
  const {quantityProducts, setQuantityProducts} = useContext(MinicartContext) as any

  const hasProduct = cart.cart?.find( (product: IProduct) => product.id === id )
  const dispatch = useCustomDispatch()
  const {callEndpoint} = useFetchAndLoad()

  const onAddProduct = () => {
    setisProcessing(true)

    setTimeout(() => {
      setisProcessing(false)
      onConfirmType()
      setAlertConfig({show: true, status: 200, text: 'Producto agregado exitosamente!'})
    }, 1000);
  }

  const onConfirmType = async () => {
    const getDataFilterProduct = await callEndpoint(getProduct(id))

    dispatch(addToCart(getDataFilterProduct.data))
    setQuantityProducts(quantityProducts + 1)
  }
  
  const onRemoveProduct = () => {
    setIsModalOpen(true)
  }

  return (
    <>
      <div className={container}>
        <p>S/ {product?.discount !== '0' ? <>{calculatePrice(product?.price, product?.discount)} <small>{product?.price}</small></> : product?.price}</p>
        <div className={containerActions}>
          {
            !hasProduct 
            ? 
            <ButtonPrimary onClick={() => onAddProduct()}>
              {isProcessing ? <BiLoaderAlt className={iconRotate} /> : 'Agregar'}
            </ButtonPrimary>
            : 
            <ButtonSimple onClick={() => onRemoveProduct()}>
              <HiOutlineTrash /> Eliminar
            </ButtonSimple>

          }
        </div>
      </div>

      <AnimatePresence initial={true}>
          {isModalOpen && <ModalActions showActions={hasActions} onClose={() => setIsModalOpen(false)} product={product && product.id} />}
      </AnimatePresence>
    </>
  )
}
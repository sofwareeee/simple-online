
import Styles from './ProductImage.module.scss'

interface Props extends React.ComponentPropsWithRef<'div'> {
  id        : string
  image     : string
  location  : string
}

export const ProductImage = ({id, image, location, ...props}: Props) => {
  const {container, containerImage} = Styles

  return (
    <div className={container} {...props}>
      <div className={containerImage} style={{backgroundImage: `url(${image})`}} />
    </div>
  )
}

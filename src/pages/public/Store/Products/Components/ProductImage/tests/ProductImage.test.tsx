import React from 'react'
import { shallow } from 'enzyme'
import { ProductImage } from '../ProductImage'

describe('ProductImage Component', () => {
  it('render product image', () => {
    const id = '1'
    const image = 'example.jpg'
    const location = 'top'

    const wrapper = shallow(<ProductImage id={id} image={image} location={location} />)

    expect(wrapper.exists()).toBe(true)
    expect(wrapper.prop('id')).toBe(id)
    expect(wrapper.prop('style')).toHaveProperty('backgroundImage', `url(${image})`)
  })
})
import React from 'react'

import Styles from './Card.module.scss'
import { TYPE_CARD } from '../../../../../../utilities/constants'

interface Props extends React.ComponentPropsWithRef<'div'> {
  children?: React.ReactNode
  type: string
}

export const Card = ({children, type, ...props}: Props) => {
  const { container, containerHorizontal } = Styles
  
  return (
    <div className={`${container} ${type === TYPE_CARD.HORIZONTAL && containerHorizontal}`} {...props}>
      {children}
    </div>
  )
}

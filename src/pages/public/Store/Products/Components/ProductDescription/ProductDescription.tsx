import Styles from './ProductDescription.module.scss'

interface Props {
  description: string
}

export const ProductDescription = ({description}: Props) => {
    const {container} = Styles
    
  return (
    <p className={container}>{description}</p>
  )
}

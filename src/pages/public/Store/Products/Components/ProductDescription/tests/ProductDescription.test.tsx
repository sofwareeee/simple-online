import React from 'react'
import { shallow } from 'enzyme'
import { ProductDescription } from '../ProductDescription'

describe('ProductDescription Component', () => {
  it('render description', () => {
    const description = 'Description to test'

    const wrapper = shallow(<ProductDescription description={description} />)

    expect(wrapper.exists()).toBe(true)
    expect(wrapper.text()).toBe(description)
  })
})
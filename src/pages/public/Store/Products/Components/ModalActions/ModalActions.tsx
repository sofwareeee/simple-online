import { motion } from 'framer-motion'

import Styles from './ModalActions.module.scss'
import { removeItem } from '../../../../../../redux/slices/cart'
import { useCustomDispatch } from '../../../../../../hooks/useRedux'
import { useContext } from 'react'
import { AlertContext } from '../../../../../../context/AlertContext'

interface Props{
  showActions: boolean
  product: any
  onClose: () => void
}

export const ModalActions = ({showActions = true, product, onClose}:Props) => {
  const {container,
    containerModal,
    modalBody, modalFooter,
    containerClose} = Styles  

    const {setAlertConfig} = useContext(AlertContext) as any
    const dispatch = useCustomDispatch()
    
    const onAccept = () => {
      dispatch(removeItem(product))
      setAlertConfig({show: true, status: 400, text: 'Producto eliminado exitosamente!'})
      onClose()
    }
    
  return (
    <div className={container}>
      <motion.div
        className={containerModal}
        initial={{ opacity: 0, y: -100 }} animate={{ opacity: 1, y: 0 }} transition={{ duration: 0.5, delay: 0.2}} exit={{ opacity: 0, y: -100 }}
      >
        <div className={modalBody}>
          <h3>Estás a punto de eliminar un producto</h3>
          <p>
            <strong>¿Está seguro?</strong></p>
        </div>
        { showActions &&
          <div className={modalFooter}>
            <div>
              <button onClick={() => onAccept()}>ACEPTAR</button>
              <button onClick={() => onClose()}>CANCELAR</button>
            </div>
          </div>
        }
      </motion.div>
      <div className={containerClose} onClick={() => onClose()} />
    </div>
  )
}

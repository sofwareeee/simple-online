import {useContext, useEffect, useState} from 'react'

import { IProduct } from '../../../../models/Product.interface'
import { TYPE_CARD } from '../../../../utilities/constants'
import { ProductCard } from './Container'
import Styles from './Products.module.scss'
import { MinicartContext } from '../../../../context/MinicartContext'
import useFetchAndLoad from '../../../../hooks/useFetchAndLoad'
import { getProducts } from '../../../../services/products/products.service'
import { Skeleton } from '../../../../components'

export const Products = () => {
  const {container, containerTitle, containerSkeleton} = Styles

  const {setOpenMinicart} = useContext(MinicartContext) as any

  const [isLoading, setIsLoading] = useState(true)
  const [productData, setproductData] = useState<IProduct[]>([])
  const {callEndpoint} = useFetchAndLoad()

  const getAllProducts = async () => {
    setIsLoading(true)
    let api_products: any =  await callEndpoint( getProducts())
    setproductData(api_products.data)
    setIsLoading(false)
  }

  useEffect(() => {
    setOpenMinicart(false)
    getAllProducts()
  }, [])

  return (
    <>
    <div className={container} >
      <div className={containerTitle}>
        <h3>Productos</h3>
      </div>
      {
      isLoading 
        ? <div className={containerSkeleton}><Skeleton /></div>
        : productData && productData.map( (product: IProduct) => {
          return <ProductCard key={product.id} product={product} style={{width: '25%'}} type={TYPE_CARD.VERTICAL} />
        })
      }
    </div>
    </>
  )
}

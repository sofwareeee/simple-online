import { useContext } from 'react'
import { AnimatePresence } from 'framer-motion'
import { Products } from './Products/Products'
import { Alert, Banner, Header } from '../../../components'
import { Minicart } from '..'
import { MinicartContext } from '../../../context/MinicartContext'
import { AlertContext } from '../../../context/AlertContext'
import { Container } from './StoreStyles'

export const Store = () => {
  const { openMinicart } = useContext(MinicartContext) as any
  const { alertConfig } = useContext(AlertContext) as any

  return (
    <>
      <Container>
        <Header />
        <Banner />
        <Products />

        <AnimatePresence>
          {openMinicart && <Minicart />}
        </AnimatePresence>
      </Container>

      <AnimatePresence>
        {alertConfig.show && <Alert />}
      </AnimatePresence>
    </>
  )
}

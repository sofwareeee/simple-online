import styled from 'styled-components';

export const Container = styled.div`
  padding: 60px 8%;
  background-image: url('/background.png');
  background-size: contain;
`;
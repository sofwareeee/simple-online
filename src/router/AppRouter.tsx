import { Routes, Route } from 'react-router-dom'
import { Store } from '../pages/public'
import { ROUTES } from '../utilities/routes'

export const AppRouter = () => {
  return (
    <Routes>
      <Route path={ROUTES.PUBLIC.STORE} element={<Store />} />

      <Route path={ROUTES.PUBLIC.HOME} element={<Store />} />

    </Routes>
  )
}

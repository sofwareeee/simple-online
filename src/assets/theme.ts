export const colors = {
    color_primary   : '#007bff',
    color_secondary : '#ff5733',
    color_tertiary  : '#f5f5f5',
    
    /* STATES */
    success         : '#33e4b2',
    danger          : '#f5dadc'
};
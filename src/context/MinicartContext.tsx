import React, { createContext, useEffect, useState } from 'react'
import { IProduct } from '../models/Product.interface'
import { saveProductInStorage } from '../pages/public/Store/Products/Utilities/functions'

interface Props{
    children: React.ReactNode
}

export const MinicartContext = createContext({})

export const MinicartContextProvider = ({children}:Props) => {
  const [productInCart, setProductInCart] = useState<IProduct[]>([])
  const [openMinicart, setOpenMinicart] = useState(false)
  const [quantityProducts, setQuantityProducts] = useState(0)

  useEffect(() => {
    if (productInCart.length <= 0) return

    saveProductInStorage(productInCart)
  }, [productInCart])
    
  return (
    <MinicartContext.Provider value={{openMinicart, setOpenMinicart, productInCart, setProductInCart, quantityProducts, setQuantityProducts}}>
      {children}
    </MinicartContext.Provider>
  )
}
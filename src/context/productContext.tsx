import React, {useState, createContext} from 'react'

interface Props{
    children: React.ReactNode
}

export const ProductContext = createContext({})

export const ProductContextProvider = ({children}: Props) => {
  const [ProductInCart, setProductInCart] = useState([])
  return (
    <ProductContext.Provider value={{ProductInCart, setProductInCart}}>
        {children}
    </ProductContext.Provider>
  )
}

import {createContext, useState} from 'react'

interface Props {
  children: React.ReactNode
}

export const ProductSelectedContex = createContext({})

export const ProductSelectedContextProvider = ({children}: Props) => {
  const [productId, setProductId] = useState('')
  const [showModalProduct, setShowModalProduct] = useState(false)

  return (
    <ProductSelectedContex.Provider value={{productId, setProductId, showModalProduct, setShowModalProduct}}>
      {children}
    </ProductSelectedContex.Provider>
  )
}

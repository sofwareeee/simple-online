import {createContext, useState} from 'react'

interface Props {
    children: React.ReactNode
}

interface IAlert {
    text    : string
    status  : number
    show    : boolean
}

const initialStatus: IAlert = {
    text    : 'Alert stared',
    status  : 100,
    show    : false
}

export const AlertContext = createContext({})

export const AlertContextProvider = ({children}: Props) => {
    const [alertConfig, setAlertConfig] = useState(initialStatus)

  return (
    <AlertContext.Provider value={{alertConfig, setAlertConfig}}>
        {children}
    </AlertContext.Provider>
  )
}

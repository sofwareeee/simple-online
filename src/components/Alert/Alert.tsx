import {useContext, useEffect} from 'react'

import { AlertContext } from '../../context/AlertContext'
import { AlertContainer, AlertText, AlertError, AlertIcon, AlertSuccess } from './Alert.styles'

export const Alert = () => {
  const { alertConfig, setAlertConfig } = useContext(AlertContext) as any;

  useEffect(() => {
    if (alertConfig.status) {
      setTimeout(() => {
        setAlertConfig({ ...alertConfig, show: false });
      }, 2000);
    }
  }, [alertConfig]);

  const getAlertContainer = () => {
    if (alertConfig.status === 200) {
      return AlertSuccess;
    } else {
      return AlertError;
    }
  };

  const AlertContainerComponent = getAlertContainer();

  return (
    <AlertContainerComponent
      as={AlertContainer}
      initial={{ opacity: 0, y: 100 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.2 }}
      exit={{ opacity: 0, y: 100 }}
    >
      <AlertText>{alertConfig.text}</AlertText>
      <AlertIcon />
    </AlertContainerComponent>
  )
}

import styled from 'styled-components';
import { motion } from 'framer-motion';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import { colors } from '../../assets/theme';

export const AlertContainer = styled(motion.div)`
  position: fixed;
  left: 30px;
  bottom: 30px;
  padding: 10px 15px;
  padding-left: 20px;
  border-radius: 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  z-index: 9999999;
  @media (max-width: 768px) {
    width: 90%;
    left: 0;
    right: 0;
    margin: auto;
    bottom: 20px;
  }
`;

export const AlertText = styled.p`
  font-size: 14px;
  font-weight: bold;
`;

export const AlertIcon = styled(AiOutlineCloseCircle)`
  font-size: 20px;
  margin-left: 20px;
`;

export const AlertSuccess = styled.div`
  background-color: ${colors.success};
  p {
    color: #008b64;
  }
  svg {
    color: #008b64;
  }
`;

export const AlertError = styled.div`
  background-color: ${colors.danger};
  p {
    color: #b45158;
  }
  svg {
    color: #b45158;
  }
`;
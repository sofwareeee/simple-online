
import Styles from './Skeleton.module.scss'

export const Skeleton = () => {
    const {container, containerImage, containerName, containerDescription, containerPrice } = Styles
  return (
    <div className={container}>
        <div className={containerImage} />
        <div className={containerName} />
        <div className={containerDescription} />
        <div className={containerPrice} />
    </div>
  )
}

import styled from 'styled-components';
import { motion } from 'framer-motion';

export const Container = styled(motion.div)`
  margin-top: 60px;
  position: relative;
  margin-bottom: 50px;
  @media (max-width: 768px) {
    margin-top: 80px;
    margin-bottom: 20px;
  }
`;

export const ContainerImage = styled.div`
  height: 60vh;
  width: 100%;
  border-radius: 20px;
  background-image: url('/images/Banner/banner.webp');
  background-position: center;
  background-size: cover;
  @media (max-width: 768px) {
    height: 25vh;
  }
`;

export const rotateAnimation = `
  @-webkit-keyframes rotate {
    to {
      transform: rotate(360deg);
    }
  }
  @keyframes rotate {
    to {
      transform: rotate(360deg);
    }
  }
  @-webkit-keyframes rotateReverse {
    to {
      transform: rotate(-360deg);
    }
  }
  @keyframes rotateReverse {
    to {
      transform: rotate(-360deg);
    }
  }
`;
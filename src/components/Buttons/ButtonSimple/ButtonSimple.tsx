import React from 'react'
import Styles from './ButtonSimple.module.scss'

interface Props extends React.ComponentPropsWithRef<'button'> {
  children: React.ReactNode
}

export const ButtonSimple = ({children, ...props}: Props) => {
  const {buttonSimple} = Styles

  return (
    <button className={buttonSimple} {...props}>
      {children}
    </button>
  )
}

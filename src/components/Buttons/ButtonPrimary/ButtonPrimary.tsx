import Styles from './ButtonPrimary.module.scss'

interface Props extends React.ComponentPropsWithRef<'button'> {
  children: React.ReactNode
}

export const ButtonPrimary = ({children, ...props}: Props) => {
  const {buttonPrimary} = Styles
    
  return (
    <button className={buttonPrimary} {...props}>
      {children}
    </button>
  )
}

import { useContext, useEffect, useState } from 'react';
import { motion } from 'framer-motion'
import Styles from './Header.module.scss'
import { NavLink } from 'react-router-dom'
import { FiShoppingCart } from 'react-icons/fi';
import { MinicartContext } from '../../context/MinicartContext';
import { getQuantityMinicart } from '../../pages/public/Store/Products/Utilities/functions';
import { useCustomSelector } from '../../hooks/useRedux';

export const Header = () => {
  const { 
      containerHeader, minicart } = Styles

  const {setOpenMinicart} = useContext(MinicartContext) as any
  const [quantityProducts, setQuantityProducts] = useState(0)
  const {cart}: any = useCustomSelector((state) => state)

  useEffect(() => {
    setQuantityProducts(getQuantityMinicart(cart.cart))
  }, [])

  useEffect(() => {
    setQuantityProducts(getQuantityMinicart(cart.cart))
  }, [cart])
  
  return (
    <motion.header 
        className={containerHeader} 
        initial={{ opacity: 0, y: -100 }} animate={{ opacity: 1, y: 0 }} transition={{ duration: 0.5, delay: 0.2}}>
        <NavLink to={'/'} ><img src="https://cdn3d.iconscout.com/3d/premium/thumb/store-5225448-4369738.png?f=webp" alt="Logo Primary" /></NavLink>
        <ul>
          <li>
            <div className={minicart} onClick={() => setOpenMinicart(true)}>
              <FiShoppingCart />
              <span>{quantityProducts}</span>
            </div>
          </li>
        </ul>
    </motion.header>
  )
}

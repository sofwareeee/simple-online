import { initialState } from './../initialStates/index';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setProducts: (state, action: PayloadAction<any>) => {
      state.data = action.payload;
    }
  }
});

export const { setProducts } = productsSlice.actions;

export default productsSlice.reducer;
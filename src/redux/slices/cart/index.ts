import { createSlice } from '@reduxjs/toolkit';
import { IProduct } from '../../../models/Product.interface';

const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    cart: [],
  },
  reducers: {
    addToCart: (state:any, action) => {
        const itemInCart = state.cart.find((item:IProduct) => item?.id === action?.payload?.id);
        if (itemInCart) {
          itemInCart.quantity++;
        } else {
          state.cart.push({ ...action.payload, quantity: 1 });
        }
    },
    removeItem: (state, action) => {
        const removeItem = state.cart.filter((item:any) => item.id !== action.payload);
        state.cart = removeItem;
    },
    removeAll: (state) => {
      state.cart = []
    }
  }
});

export const { 
    addToCart,
    removeItem,
    removeAll } = cartSlice.actions;

export default cartSlice.reducer;
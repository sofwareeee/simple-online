import axios from 'axios'
import { APP } from '../config/app'

const axiosInstance = axios.create()

axiosInstance.defaults.baseURL = APP.API_URL

export default axiosInstance
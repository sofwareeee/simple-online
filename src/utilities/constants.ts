
export const TYPE_CARD = {
  VERTICAL: 'VERTICAL',
  HORIZONTAL: 'HORIZONTAL'
}

export const STORAGE = {
  MINICART: '____CART'
}

export const loadAbort = () =>{
    const controller = new AbortController();
    return controller;
}

export const calculateTotal = (minicart: any) => {
    let priceTotal = 0
    
    minicart.forEach((product: any) => {
        if (product.discount !== '0') {
            priceTotal += (Number(product.price) - (Number(product.price) * ( Number(product.discount) / 100 ) )) * Number(product.quantity)
        } else {
            priceTotal += Number(product.price) * Number(product.quantity)
        }

    });

    return priceTotal.toFixed(2)
}
export const VERSION = {
    INFO: 'v1'
}

export const ROUTES = {
    PUBLIC: {
        HOME: '/',
        STORE: '/store',
    }
}
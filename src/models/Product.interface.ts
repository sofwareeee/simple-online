export interface IProduct {
  id: string
  name: string
  description: string
  image: string
  size: string
  presentation?: string | undefined
  price: number | string | undefined
  discount: number | string
  content: Content[] | string[]
  quantity?: number | undefined
}

export interface Content {
  bowl: Bowl[]
  salsa: Salsa[]
  sandwich: Sandwich[]
  bebida: Bebida[]
}

export interface Bowl {
  name: string
  productId: number[]
  attributes: IAttributes[]
  maxTopping: number
}

export interface FrutosSeco {
  name: string
  productId: number[]
  attributes: IAttributes[]
  maxTopping: number
}

export interface Salsa {
  name: string
  productId: number[]
  attributes: IAttributes[]
  maxTopping: number
}

export interface Sandwich {
  name: string
  productId: number[]
  attributes: IAttributes[]
  maxTopping: number
}

export interface Bebida {
  name: string
  productId: number[]
  attributes: IAttributes[]
  maxTopping: number
}

export interface IAttributes {
  id: number
  name: string
}
import { BrowserRouter } from 'react-router-dom'
import { AppRouter } from './router/AppRouter'
import { AnimatePresence } from 'framer-motion'
import { MinicartContextProvider } from './context/MinicartContext'
import { ProductSelectedContextProvider } from './context/ProductSelectedContextProvider'
import { Provider } from 'react-redux'
import store, { persistor } from './redux/store'
import { PersistGate } from 'redux-persist/integration/react'
import { AlertContextProvider } from './context/AlertContext'

function App() {

  return (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <ProductSelectedContextProvider>
        <MinicartContextProvider>
            <AnimatePresence>
              <AlertContextProvider>
                <BrowserRouter>
                  <AppRouter />
                </BrowserRouter>
              </AlertContextProvider>
            </AnimatePresence>
        </MinicartContextProvider>
      </ProductSelectedContextProvider>
    </PersistGate>
  </Provider>
  )
}

export default App

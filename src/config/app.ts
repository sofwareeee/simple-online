export const APP = {
    ENV: import.meta.env.VITE_ENV,
    API_URL: import.meta.env.VITE_API
}